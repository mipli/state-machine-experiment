use super::Bottle;

pub enum MachineEvent {
    Nop,
    InsertBottle { bottle: Bottle },
    BottleFull,
    BottleEjected,
}

#[derive(Debug, Clone)]
pub enum Machine {
    Waiting,
    Filling { bottle: Bottle },
    Done { bottle: Bottle },
}

impl Machine {
    pub fn next(self, event: MachineEvent) -> Result<Self, String> {
        match (&self, event) {
            (Machine::Filling { mut bottle }, MachineEvent::Nop) => {
                bottle.volume += 1;
                Ok(Machine::Filling { bottle })
            }
            (Machine::Waiting, MachineEvent::InsertBottle { bottle }) => {
                Ok(Machine::Filling { bottle })
            }
            (Machine::Filling { bottle }, MachineEvent::BottleFull) => {
                Ok(Machine::Done { bottle: *bottle })
            }
            (Machine::Done { .. }, MachineEvent::BottleEjected) => Ok(Machine::Waiting),
            (_, MachineEvent::Nop) => Ok(self),
            (_, _) => Err("Invalid transition".to_string()),
        }
    }

    pub fn run(&self) {
        match self {
            Machine::Waiting => println!("Machine waiting"),
            Machine::Filling { bottle } => println!("Machine is filling: {:?}", bottle),
            Machine::Done { bottle } => println!("Machine done: {:?}", bottle),
        }
    }
}
