use super::Bottle;

pub trait StateMachine {
    type State;
    type Transition;
    type Output;

    fn transition(
        &mut self,
        transition: Self::Transition,
    ) -> Result<Self::Output, StateMachineError>;
}

#[derive(Debug)]
pub enum StateMachineError {
    InvalidTransition,
}

pub enum MachineState {
    Waiting,
    Filling { bottle: Bottle },
    Done { bottle: Bottle },
}

pub enum MachineEvent {
    InsertBottle { bottle: Bottle },
    BottleFull,
    BottleEjected,
}

pub enum MachineOutput {
    None,
    Bottle { bottle: Bottle },
}

pub struct Machine {
    pub state: MachineState,
}

impl Machine {
    pub fn new() -> Self {
        Machine {
            state: MachineState::Waiting,
        }
    }

    pub fn tick(&mut self) -> String {
        match self.state {
            MachineState::Waiting => "Machine waiting".to_string(),
            MachineState::Filling { ref mut bottle } => {
                bottle.volume += 1;
                format!("Machine is filling: {bottle:?}")
            }
            MachineState::Done { bottle } => format!("Machine done: {bottle:?}"),
        }
    }
}

impl StateMachine for Machine {
    type State = MachineState;
    type Transition = MachineEvent;
    type Output = MachineOutput;

    fn transition(
        &mut self,
        transition: Self::Transition,
    ) -> Result<Self::Output, StateMachineError> {
        match (&self.state, transition) {
            (MachineState::Waiting, MachineEvent::InsertBottle { bottle }) => {
                self.state = MachineState::Filling { bottle };
                Ok(MachineOutput::None)
            }
            (MachineState::Filling { bottle }, MachineEvent::BottleFull) => {
                self.state = MachineState::Done { bottle: *bottle };
                Ok(MachineOutput::None)
            }
            (MachineState::Done { bottle }, MachineEvent::BottleEjected) => {
                let output = MachineOutput::Bottle { bottle: *bottle };
                self.state = MachineState::Waiting;
                Ok(output)
            }
            (_, _) => Err(StateMachineError::InvalidTransition),
        }
    }
}
