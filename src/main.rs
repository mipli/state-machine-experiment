#![feature(adt_const_params)]

mod enum_version;
mod event_version;
mod struct_version;
mod trait_version;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Bottle {
    max_volume: usize,
    volume: usize,
}

impl Bottle {
    pub fn new(max_volume: usize) -> Self {
        Bottle {
            max_volume,
            volume: 0,
        }
    }
}

mod enum_usage {
    use super::enum_version::{FillResult, Machine, State};
    use super::Bottle;

    fn add_bottle(machine: Machine<{ State::Waiting }>) -> Machine<{ State::Done }> {
        let bottle = Bottle::new(5);
        log::info!("Machine state: {:?}", &machine.state());

        let mut machine = machine.insert_bottle(bottle);
        log::info!("Machine state: {:?}", &machine.state());

        let machine = loop {
            match machine.fill() {
                FillResult::Continue(m) => machine = m,
                FillResult::Done(m) => {
                    break m;
                }
            }
            log::info!("Machine state: {:?}", &machine.state());
            log::info!("Bottle state: {:?}", machine.bottle);
        };
        log::info!("Machine state: {:?}", &machine.state());
        machine
    }

    pub fn run() {
        let machine = Machine::new(1);
        let machine = add_bottle(machine);
        let (machine, bottle) = machine.remove_bottle();
        log::info!("Filled bottle: {:?}", bottle);
        let machine = add_bottle(machine);
        let (_machine, bottle) = machine.remove_bottle();
        log::info!("Filled bottle: {:?}", bottle);
    }
}

mod struct_usage {
    use super::struct_version::{DoneState, FillResult, Machine, WaitingState};
    use super::Bottle;

    fn add_bottle(machine: Machine<WaitingState>) -> Machine<DoneState> {
        let bottle = Bottle::new(5);
        log::info!("Machine state: {:?}", &machine.state());

        let mut machine = machine.insert_bottle(bottle);
        log::info!("Machine state: {:?}", &machine.state());

        let machine = loop {
            match machine.fill() {
                FillResult::Continue(m) => machine = m,
                FillResult::Done(m) => {
                    break m;
                }
            }
            log::info!("Machine state: {:?}", &machine.state());
        };
        log::info!("Machine state: {:?}", &machine.state());
        machine
    }

    pub fn run() {
        let machine = Machine::new(1);
        let machine = add_bottle(machine);
        let (machine, bottle) = machine.remove_bottle();
        log::info!("Filled bottle: {:?}", bottle);
        let machine = add_bottle(machine);
        let (_machine, bottle) = machine.remove_bottle();
        log::info!("Filled bottle: {:?}", bottle);
    }
}

mod event_usage {
    use super::event_version::{Machine, MachineEvent};
    use super::Bottle;

    pub fn run() {
        let mut bottles = vec![];

        let mut machine = Machine::Waiting;

        while bottles.len() < 2 {
            let event = match machine {
                Machine::Waiting => MachineEvent::InsertBottle {
                    bottle: Bottle::new(5),
                },
                Machine::Filling { bottle } => {
                    if bottle.volume < bottle.max_volume {
                        MachineEvent::Nop
                    } else {
                        MachineEvent::BottleFull
                    }
                }
                Machine::Done { .. } => MachineEvent::BottleEjected,
            };
            machine = machine.next(event).expect("Invalid transition");
            machine.run();
            if let Machine::Done { bottle } = machine {
                bottles.push(bottle);
            }
        }
        log::info!("Filled bottles: {:?}", bottles);
    }
}

mod trait_usage {
    use super::trait_version::{Machine, MachineEvent, MachineOutput, MachineState, StateMachine};
    use super::Bottle;

    pub fn run() {
        let mut bottles = vec![];

        let mut machine = Machine::new();

        while bottles.len() < 2 {
            let event = match machine.state {
                MachineState::Waiting => Some(MachineEvent::InsertBottle {
                    bottle: Bottle::new(5),
                }),
                MachineState::Filling { bottle } => {
                    if bottle.volume < bottle.max_volume {
                        None
                    } else {
                        Some(MachineEvent::BottleFull)
                    }
                }
                MachineState::Done { .. } => Some(MachineEvent::BottleEjected),
            };
            if let Some(event) = event {
                if let MachineOutput::Bottle { bottle } =
                    machine.transition(event).expect("Invalid transition")
                {
                    bottles.push(bottle);
                }
            }
            let output = machine.tick();
            log::info!("{}", output);
        }
        log::info!("Filled bottles: {:?}", bottles);
    }
}

fn main() {
    env_logger::init();

    log::info!("### Running Const Enum version");
    enum_usage::run();

    log::info!("### Running Struct version");
    struct_usage::run();

    log::info!("### Running Event version");
    event_usage::run();

    log::info!("### Running Trait version");
    trait_usage::run();
}
