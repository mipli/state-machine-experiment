use super::Bottle;

pub trait MachineState {}

#[derive(Debug, Clone, Copy)]
pub struct WaitingState;

#[derive(Debug, Clone, Copy)]
pub struct FillingState {
    bottle: Bottle,
}
#[derive(Debug, Clone, Copy)]
pub struct DoneState {
    bottle: Bottle,
}

impl MachineState for WaitingState {}
impl MachineState for FillingState {}
impl MachineState for DoneState {}

pub struct Machine<S: MachineState + Copy + std::fmt::Debug> {
    rate: usize,
    state: S,
}

impl<S: MachineState + Copy + std::fmt::Debug> Machine<S> {
    pub fn state(&self) -> S {
        self.state
    }
}

impl Machine<WaitingState> {
    pub fn new(rate: usize) -> Self {
        Machine {
            rate,
            state: WaitingState,
        }
    }
    pub fn insert_bottle(self, bottle: Bottle) -> Machine<FillingState> {
        Machine {
            rate: self.rate,
            state: FillingState { bottle },
        }
    }
}

pub enum FillResult {
    Continue(Machine<FillingState>),
    Done(Machine<DoneState>),
}

impl Machine<FillingState> {
    pub fn fill(mut self) -> FillResult {
        let FillingState { ref mut bottle } = self.state;
        if bottle.volume + self.rate > bottle.max_volume {
            bottle.volume = bottle.max_volume;
            FillResult::Done(Machine::<DoneState> {
                rate: self.rate,
                state: DoneState { bottle: *bottle },
            })
        } else {
            bottle.volume += self.rate;
            FillResult::Continue(self)
        }
    }
}

impl Machine<DoneState> {
    pub fn remove_bottle(self) -> (Machine<WaitingState>, Bottle) {
        let DoneState { bottle } = self.state;
        (
            Machine::<WaitingState> {
                rate: self.rate,
                state: WaitingState,
            },
            bottle,
        )
    }
}
