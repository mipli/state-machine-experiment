use super::Bottle;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum State {
    Waiting,
    Filling,
    Done,
}

#[derive(Debug, Clone, Copy)]
pub struct Machine<const S: State> {
    rate: usize,
    // a downside of this approach is that the bottle value is preset in all states,
    // while we know it's empty in the waiting state, and fillled in the other two states
    pub bottle: Option<Bottle>,
}

impl Machine<{ State::Waiting }> {
    pub fn new(rate: usize) -> Self {
        Machine { rate, bottle: None }
    }

    pub fn insert_bottle(self, bottle: Bottle) -> Machine<{ State::Filling }> {
        Machine {
            rate: self.rate,
            bottle: Some(bottle),
        }
    }

    pub fn state(&self) -> State {
        State::Waiting
    }
}

pub enum FillResult {
    Continue(Machine<{ State::Filling }>),
    Done(Machine<{ State::Done }>),
}

impl Machine<{ State::Filling }> {
    pub fn fill(self) -> FillResult {
        let mut bottle = self.bottle.unwrap();
        if bottle.volume + self.rate > bottle.max_volume {
            bottle.volume = bottle.max_volume;
            FillResult::Done(Machine {
                rate: self.rate,
                bottle: Some(bottle),
            })
        } else {
            bottle.volume += self.rate;
            FillResult::Continue(Machine {
                rate: self.rate,
                bottle: Some(bottle),
            })
        }
    }

    pub fn state(&self) -> State {
        State::Filling
    }
}

impl Machine<{ State::Done }> {
    pub fn remove_bottle(self) -> (Machine<{ State::Waiting }>, Bottle) {
        (
            Machine {
                rate: self.rate,
                bottle: None,
            },
            self.bottle.unwrap(),
        )
    }

    pub fn state(&self) -> State {
        State::Done
    }
}
