# State Machine experimentation

See [something](https://mipli.gitlab.io/..) for my blog post about this experiment.

## Experiments

### struct_version.rs

Probably the most common way of creating a state machine, defined as a struct generic over "state structs". Well defined transitions between states which the compiler can verify which I feel is one of the main requirements for a good state machine.

Data that's unique to each state can easily be stored in the state structs as well, which creates a nice separation between information relevant for all states and the infomation that's only relevant for a single state.

Biggest issue I have with this solution is that it feels more natural to have the various state defined as an enum, since that allows for exhausting matching over the possible states.

### event_version.rs

A state machine defined as an enum, with state changes triggered by an event enum.

Elegent solution with using events to trigger the state changes, and it also allows for unique data for each state stored in the state enums.

A big, and for me fairly critical issue, with this solution is the fact that there can be invalid state changes that the compiler cannot catch and we need a seprate state for that. Rust has such a powerful type system, so it should be possible to avoid that.

### enum_version.rs

Using the new `adt_const_param` feature to specify the state machine as a struct generic over the enum's variations.

Defining the various states as enums is more elegant than using structs for it. This solution lacks a good way of storing information that's unique for each state though, so we're forced to have some extra `Option<..>` fields on the machine struct to store the state information.

## Conclusion

In the end I think I prefer the traditional 'struct_version' for it's ability to store state information in the state struct, and we can use the compiler to guarantee that all state transitions are valid.

If you end up in a situation where the combination of events you need to handle and the possible machine states makes it impossible for the compiler to verify valid transitions the `event_version` seems like a good solution. Would still prefer to avoid it though if possible.

The new solution using 'adt_const_param' doens't seem to offer much over the original 'struct_version', at least not enough that it's worth losing the option to store unique data for each state. If you know your application will only ever need state machines without unique data per state this would be a good solution, but rather than mixing the various types I would just go with the `struct_version` in an application that needs both.
